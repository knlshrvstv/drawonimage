//
//  ViewController.h
//  charades
//
//  Created by Kunal Shrivastava on 2/9/15.
//  Copyright (c) 2015 hifileo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HFLPhotoViewController.h"

@interface HFLMainViewController : UIViewController <UIActionSheetDelegate,HFLPhotoViewControllerDelegate>
@property (strong, nonatomic) IBOutlet UIImageView *origImage;
@property (strong, nonatomic) IBOutlet UIImageView *artImage;

- (IBAction)pencilPressed:(id)sender;

- (IBAction)eraserPressed:(id)sender;



@end

