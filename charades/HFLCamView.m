//
//  HFLCamView.m
//  charades
//
//  Created by Kunal Shrivastava on 2/9/15.
//  Copyright (c) 2015 hifileo. All rights reserved.
//

#import "HFLCamView.h"
#import <AVFoundation/AVFoundation.h>

@implementation HFLCamView

+ (Class)layerClass
{
    return [AVCaptureVideoPreviewLayer class];
}

- (AVCaptureSession *)session
{
    return [(AVCaptureVideoPreviewLayer *)[self layer] session];
}

- (void)setSession:(AVCaptureSession *)session
{
    [(AVCaptureVideoPreviewLayer *)[self layer] setSession:session];
}

@end
