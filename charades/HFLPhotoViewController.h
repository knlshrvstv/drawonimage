//
//  HFLPhotoViewController.h
//  charades
//
//  Created by Kunal Shrivastava on 2/17/15.
//  Copyright (c) 2015 hifileo. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HFLPhotoViewControllerDelegate <NSObject>
- (void)fromHFLPhotoViewController:(UIImageView*)imageFromLib;
@end

@interface HFLPhotoViewController : UIViewController <UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property (strong, nonatomic) IBOutlet UIImageView *imageView;

@property (nonatomic, weak) id <HFLPhotoViewControllerDelegate> delegate;

- (IBAction)takePhoto:(id)sender;

- (IBAction)selectPhoto:(id)sender;

- (IBAction)getPhoto:(id)sender;
@end
