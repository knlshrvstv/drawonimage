//
//  AppDelegate.h
//  charades
//
//  Created by Kunal Shrivastava on 2/9/15.
//  Copyright (c) 2015 hifileo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HFLAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

