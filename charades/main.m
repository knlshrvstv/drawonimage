//
//  main.m
//  charades
//
//  Created by Kunal Shrivastava on 2/9/15.
//  Copyright (c) 2015 hifileo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HFLAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([HFLAppDelegate class]));
    }
}
