//
//  HFLCamView.h
//  charades
//
//  Created by Kunal Shrivastava on 2/9/15.
//  Copyright (c) 2015 hifileo. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AVCaptureSession;

@interface HFLCamView : UIView

@property (nonatomic) AVCaptureSession *session;

@end
